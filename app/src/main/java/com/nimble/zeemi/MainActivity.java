package com.nimble.zeemi;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener {
    public static final String API = "https://api-staging.zeemi.tv";
    public static final String TOKEN = "9008ab338d1beba78b8914124d64d461a9a9253894b29ea5cd70a0cf9c955177";
    ViewPager tabContentPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.main_tab);

        tabContentPager = (ViewPager) findViewById(R.id.main_pager);
        TabContentPagerAdapter tabContentAdapter = new TabContentPagerAdapter(this, getSupportFragmentManager());
        tabContentPager.setAdapter(tabContentAdapter);
        tabContentPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabContentPager.setOffscreenPageLimit(2);

        tabLayout.setTabsFromPagerAdapter(tabContentAdapter);
        tabLayout.setOnTabSelectedListener(this);

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        tabContentPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}
