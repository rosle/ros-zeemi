package com.nimble.zeemi;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nimble.zeemi.model.Channel;
import com.nimble.zeemi.model.User;
import com.squareup.picasso.Picasso;

import java.util.List;

import jp.wasabeef.picasso.transformations.CropCircleTransformation;

/**
 * Created by rossukhon on 8/22/15 AD.
 */
public class ChannelListAdapter extends RecyclerView.Adapter {
    private final static int TYPE_PROGRESS = 0;
    private final static int TYPE_DATA = 1;

    private Context mContext;
    private List<Channel> channelListData;

    public ChannelListAdapter(Context mContext, List<Channel> channelListData) {
        this.mContext = mContext;
        this.channelListData = channelListData;
    }

    public static class ChannelViewHolder extends RecyclerView.ViewHolder {
        public ImageView channelCover;
        public ImageView userAvatar;
        public TextView channelName;
        public TextView userName;
        public AppCompatRatingBar channelRating;
        public ImageView channelIconFollower;
        public TextView channelTotalFollower;

        public ChannelViewHolder(View itemView) {
            super(itemView);
            channelCover = (ImageView) itemView.findViewById(R.id.channel_cover_image);
            userAvatar = (ImageView) itemView.findViewById(R.id.channel_user_avatar);
            channelName = (TextView) itemView.findViewById(R.id.channel_name);
            userName = (TextView) itemView.findViewById(R.id.channel_user_name);
            channelRating = (AppCompatRatingBar) itemView.findViewById(R.id.channel_rating);
            channelIconFollower = (ImageView) itemView.findViewById(R.id.channel_icon_follower);
            channelTotalFollower = (TextView) itemView.findViewById(R.id.channel_total_follower);
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progress_bar);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int type) {
        RecyclerView.ViewHolder holder;
        if (type == TYPE_DATA) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fragment_channel_list_item, viewGroup, false);
            holder = new ChannelViewHolder(view);

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                LayerDrawable stars = (LayerDrawable) ((ChannelViewHolder) holder).channelRating.getProgressDrawable();
                stars.getDrawable(2).setColorFilter(ContextCompat.getColor(mContext, R.color.channel_rating_full_color),
                        PorterDuff.Mode.SRC_ATOP);
                stars.getDrawable(1).setColorFilter(ContextCompat.getColor(mContext, R.color.channel_rating_normal_color),
                        PorterDuff.Mode.SRC_ATOP);
                stars.getDrawable(0).setColorFilter(ContextCompat.getColor(mContext, R.color.channel_rating_normal_color),
                        PorterDuff.Mode.SRC_ATOP);
            }
        } else {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fragment_channel_list_progress_item, viewGroup, false);
            holder = new ProgressViewHolder(view);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof ChannelViewHolder) {
            Channel channel = channelListData.get(position);

            ((ChannelViewHolder) viewHolder).channelName.setText(channel.getName());
            ((ChannelViewHolder) viewHolder).channelRating.setRating(channel.getRating());
            Picasso.with(mContext).load(R.drawable.ic_heart)
                    .into(((ChannelViewHolder) viewHolder).channelIconFollower);
            String totalFollower = mContext.getString(R.string.channel_total_follower_format, channel.getFollowersCount() + "");
            ((ChannelViewHolder) viewHolder).channelTotalFollower.setText(totalFollower);
            String channelCover = channel.getCoverImage();
            if (channelCover != null && !channelCover.isEmpty()) {
                Picasso.with(mContext).load(channelCover)
                        .into(((ChannelViewHolder) viewHolder).channelCover);
            }

            User owner = channel.getUser();
            ((ChannelViewHolder) viewHolder).userName.setText(owner.getUsername());
            String userAvatar = owner.getProfilePicture();
            if (userAvatar != null && !userAvatar.isEmpty()) {
                Picasso.with(mContext).load(userAvatar)
                        .transform(new CropCircleTransformation())
                        .into(((ChannelViewHolder) viewHolder).userAvatar);
            } else {
                Picasso.with(mContext).load(R.drawable.default_user_avatar)
                        .transform(new CropCircleTransformation())
                        .into(((ChannelViewHolder) viewHolder).userAvatar);
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        return channelListData.get(position) != null ? TYPE_DATA : TYPE_PROGRESS;
    }

    @Override
    public int getItemCount() {
        return channelListData.size();
    }


}
