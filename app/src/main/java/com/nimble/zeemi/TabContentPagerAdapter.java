package com.nimble.zeemi;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by rossukhon on 8/22/15 AD.
 */
public class TabContentPagerAdapter extends FragmentStatePagerAdapter {

    private Context mContext;
    private int[] pageTitle = {R.string.main_tab_live,
            R.string.main_tab_upcoming,
            R.string.main_tab_popular};
    private String page[] = {ChannelListFragment.TYPE_LIVE,
            ChannelListFragment.TYPE_UPCOMING,
            ChannelListFragment.TYPE_POPULAR};

    public TabContentPagerAdapter(Context mContext, FragmentManager fm) {
        super(fm);
        this.mContext = mContext;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(pageTitle[position]);
    }

    @Override
    public Fragment getItem(int position) {
        return ChannelListFragment.newInstance(page[position]);
    }

    @Override
    public int getCount() {
        return page.length;
    }
}
