package com.nimble.zeemi.model;

/**
 * Created by rossukhon on 8/22/15 AD.
 */
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Channel {

    @Expose
    private String id;
    @Expose
    private String name;
    @Expose
    private Integer rating;
    @SerializedName("cover_image")
    @Expose
    private String coverImage;
    @Expose
    private Boolean featured;
    @Expose
    private Boolean picked;
    @SerializedName("is_hot_picked")
    @Expose
    private Boolean isHotPicked;
    @SerializedName("followers_count")
    @Expose
    private Integer followersCount;
    @SerializedName("viewers_count")
    @Expose
    private Integer viewersCount;
    @SerializedName("is_live")
    @Expose
    private Boolean isLive;
    @SerializedName("next_performance")
    @Expose
    private Object nextPerformance;
    @Expose
    private User user;

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The rating
     */
    public Integer getRating() {
        return rating;
    }

    /**
     *
     * @param rating
     * The rating
     */
    public void setRating(Integer rating) {
        this.rating = rating;
    }

    /**
     *
     * @return
     * The coverImage
     */
    public String getCoverImage() {
        return coverImage;
    }

    /**
     *
     * @param coverImage
     * The cover_image
     */
    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    /**
     *
     * @return
     * The featured
     */
    public Boolean getFeatured() {
        return featured;
    }

    /**
     *
     * @param featured
     * The featured
     */
    public void setFeatured(Boolean featured) {
        this.featured = featured;
    }

    /**
     *
     * @return
     * The picked
     */
    public Boolean getPicked() {
        return picked;
    }

    /**
     *
     * @param picked
     * The picked
     */
    public void setPicked(Boolean picked) {
        this.picked = picked;
    }

    /**
     *
     * @return
     * The isHotPicked
     */
    public Boolean getIsHotPicked() {
        return isHotPicked;
    }

    /**
     *
     * @param isHotPicked
     * The is_hot_picked
     */
    public void setIsHotPicked(Boolean isHotPicked) {
        this.isHotPicked = isHotPicked;
    }

    /**
     *
     * @return
     * The followersCount
     */
    public Integer getFollowersCount() {
        return followersCount;
    }

    /**
     *
     * @param followersCount
     * The followers_count
     */
    public void setFollowersCount(Integer followersCount) {
        this.followersCount = followersCount;
    }

    /**
     *
     * @return
     * The viewersCount
     */
    public Integer getViewersCount() {
        return viewersCount;
    }

    /**
     *
     * @param viewersCount
     * The viewers_count
     */
    public void setViewersCount(Integer viewersCount) {
        this.viewersCount = viewersCount;
    }

    /**
     *
     * @return
     * The isLive
     */
    public Boolean getIsLive() {
        return isLive;
    }

    /**
     *
     * @param isLive
     * The is_live
     */
    public void setIsLive(Boolean isLive) {
        this.isLive = isLive;
    }

    /**
     *
     * @return
     * The nextPerformance
     */
    public Object getNextPerformance() {
        return nextPerformance;
    }

    /**
     *
     * @param nextPerformance
     * The next_performance
     */
    public void setNextPerformance(Object nextPerformance) {
        this.nextPerformance = nextPerformance;
    }

    /**
     *
     * @return
     * The user
     */
    public User getUser() {
        return user;
    }

    /**
     *
     * @param user
     * The user
     */
    public void setUser(User user) {
        this.user = user;
    }

}