package com.nimble.zeemi.api;

import com.nimble.zeemi.model.Channel;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by rossukhon on 8/22/15 AD.
 */
public interface ZeemiService {

    @GET("/1/channels/{channelType}.json")
    void getChannelFeed(@Path("channelType") String channelType, @Query("page") int page, @Query("access_token") String accessToken, Callback<List<Channel>> channelList);


}
