package com.nimble.zeemi;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nimble.zeemi.api.ZeemiService;
import com.nimble.zeemi.model.Channel;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ChannelListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private static final String CHANNEL_TYPE = "channelType";
    public static final String TYPE_LIVE = "live";
    public static final String TYPE_UPCOMING = "upcoming";
    public static final String TYPE_POPULAR = "popular";

    private ZeemiService service;

    private String channelType;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ArrayList<Channel> channelListData;
    private RecyclerView channelRecyclerView;
    private RecyclerView.Adapter channelRecycleAdapter;
    private LinearLayoutManager layoutManager;

    public static ChannelListFragment newInstance(String channelType) {
        ChannelListFragment fragment = new ChannelListFragment();
        Bundle args = new Bundle();
        args.putString(CHANNEL_TYPE, channelType);
        fragment.setArguments(args);
        return fragment;
    }

    public ChannelListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            channelType = getArguments().getString(CHANNEL_TYPE, null);
        }
        channelListData = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_channel_list, container, false);

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        channelRecyclerView = (RecyclerView) view.findViewById(R.id.channel_list);
        layoutManager = new LinearLayoutManager(getActivity());
        channelRecyclerView.setLayoutManager(layoutManager);
        channelRecycleAdapter = new ChannelListAdapter(getActivity(), channelListData);
        channelRecyclerView.setAdapter(channelRecycleAdapter);
        channelRecyclerView.addOnScrollListener(recycleViewScrollListener);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(MainActivity.API).build();
        service = restAdapter.create(ZeemiService.class);
        currentPage = 0;
        loadChannelData(1);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public synchronized void loadChannelData(final int page) {
        if (page > currentPage) {
            if (!swipeRefreshLayout.isRefreshing()) {
                channelListData.add(null);
                channelRecycleAdapter.notifyItemInserted(channelListData.size() - 1);
            }

            service.getChannelFeed(channelType, page, MainActivity.TOKEN, new Callback<List<Channel>>() {

                @Override
                public void success(List<Channel> channels, Response response) {
                    if (swipeRefreshLayout.isRefreshing()) {
                        swipeRefreshLayout.setRefreshing(false);
                        channelListData.clear();
                    } else {
                        channelListData.remove(channelListData.size() - 1);
                    }

                    if (!channels.isEmpty() && page > currentPage) {
                        channelListData.addAll(channels);
                        currentPage = page;
                    }

                    channelRecycleAdapter.notifyDataSetChanged();
                    loading = false;
                }

                @Override
                public void failure(RetrofitError error) {
                    if (swipeRefreshLayout.isRefreshing()) {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                    channelListData.remove(channelListData.size() - 1);
                    loading = false;
                }
            });
        }
    }

    private int visibleThreshold = 3;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private int currentPage = 0;

    private OnScrollListener recycleViewScrollListener = new OnScrollListener() {
        private int scrollState;

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            scrollState = newState;
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if (dy > 0) {
                totalItemCount = layoutManager.getItemCount();
                lastVisibleItem = layoutManager.findLastVisibleItemPosition();

                if (!loading
                        && totalItemCount <= (lastVisibleItem + visibleThreshold)
                        && scrollState == RecyclerView.SCROLL_STATE_SETTLING) {
                    loading = true;
                    loadChannelData(currentPage + 1);
                }
            }
        }
    };

    @Override
    public void onRefresh() {
        if (!loading) {
            currentPage = 0;
            loadChannelData(1);
        }
    }
}
